<?php

/**
 * Batch file job queue.  Logs batch file send process in watchdog.
 */
function commerce_beanstream_batch_file_job_queue() {

  // Get payment method settings.
  $payment_method = commerce_payment_method_instance_load('commerce_beanstream|commerce_payment_commerce_beanstream');

  // Write date to log file.
  write_to_log("---------------" . date('Y-m-d H:i:s') . "---------------", 'batch');
  // Build batch file.
  $batch_file = commerce_beanstream_build_batch_file($payment_method);

  // Check that files exists
  if(file_exists($batch_file)) {
    // If it does record action in watchdog.
    $msg = "Batch file created successfully, check log for details.\n";
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
    write_to_log($msg, 'batch');
    // Now double check user wants to send file to beanstream.
    $option = drush_get_option('bypass', '');
    if ($option != 'yes') {
      $option = drush_choice(array('yes' => 'Yes'), dt('Would you like to send the batch file to beanstream?'));
    }
  }
  else {
    // If file build fails record in watchdog
    $msg = "Batch file creation failed. Check log for details.";
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
    write_to_log($msg, 'batch');
  }

  if(isset($option) && $option != false) {
    // Send file to beanstream.
    $send_file = _commerce_beanstream_profile_process_batch($payment_method, $batch_file);
    // Record API response in watchdog.
    if ($send_file->code == 1) {
      $msg = "File successfully sent to beanstream.";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
      write_to_log($msg, 'batch');
    }
    else {
      $msg = "Failed to send file to beanstream.";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
      write_to_log($msg, 'batch');
    }

    // Notify monthly credit card payment admin upon the success or failure of the batch file send.
    $email_admin = commerce_beanstream_notify_admin($payment_method, 'batch_file', $send_file, $log_file);
    // Record delivery success or failure of notification email in watchdog
    if ($email_admin) {
      $msg = "Notification Email to CC Admin successfully sent.";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
      write_to_log($msg, 'batch');
    }
    else {
      $msg = "Notification Email to CC Admin failed to send.";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
      write_to_log($msg, 'batch');
    }
  }
}

/**
 * Builds batch file to send to beanstream for monthly credit card payments.
 * Order details can be altered by calling: hook_commerce_beanstream_batch_file_order_details_alter
 *
 * @param $payment_method
 * @return bool|string
 */
function commerce_beanstream_build_batch_file($payment_method) {
  // Store the drush get option 'declined' flag.
  $process_declined = drush_get_option('declined', '');
  // Declare order details array.
  $order_details = array();

  // If the process declined option was not flagged create a normal batch file.
  if ($process_declined != 'yes') {
    // First things first, check if a batch has already been sent within the last 48 hrs. If so, kill process.
    $batch_files = db_query("SELECT batch_id, date_sent FROM commerce_beanstream_batch_file
                     WHERE status_message = 'File successfully received.'
                     AND date_sent BETWEEN :start AND :finish",
      array(':start' => time() - (60 * 60 * 48), ':finish' => time()));

    if ($batch_files->rowCount() > 0) {
      foreach ($batch_files as $file) {
        if (date('Y-m', $file->date_sent) == date('Y-m')) {
          // Batch file sent this month, write to log and notify.
          $msg = "You have already sent a batch file this month.  Batch ID: $file->batch_id";
          watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
          write_to_log($msg . "\nTerminationg batch process...", 'batch');

          return FALSE;
        }
      }
    }

    // Determine the end of payments date.
    $payment_end = new DateTime(date('Y') . '-' . $payment_method['settings']['recurring']['last_month_rb_cycle']
      . '-' . $payment_method['settings']['recurring']['billing_day']);

    // If the payment end date has passed when this batch is run.  We move on to the next
    // year of recurring payments.
    if (time() > $payment_end->format('U')) {
      $payment_end->modify('+1 year');
    }

    // Pull all recurring payments of current year from DB.
    $recurring_payments = db_query('SELECT * FROM commerce_beanstream_recurring_payment
                                    WHERE payment_year = :year', array(':year' => $payment_end->format('Y')));

    // Set process failed transactions flag in the order details array
    // so our hook knows to respect this.
    $order_details['process_failed_transactions'] = FALSE;
  }
  else {
    // If the proccess declined option was set process only declined transactions
    // for current month.
    write_to_log('Processings this month failed transactions', 'batch');
    $batch_file = db_query("SELECT batch_id FROM commerce_beanstream_batch_file 
                            WHERE FROM_UNIXTIME(date_sent, '%M') = :current_month",
                            array(':current_month' => date('F')))->fetchField();

    if (!$batch_file) {
      // Batch has not been sent this month, write to log and notify.
      $msg = "You cannot reprocess cancelled transactions because a batch file has not been sent this month.";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
      write_to_log($msg . "\nTerminationg batch process...", 'batch');
      return FALSE;
    }

    // Pull all transactions from this month's batch file with a status of declined.
    $failed_transactions = db_query("SELECT * FROM commerce_beanstream_recurring_payment
                                     WHERE batch_file_id = :batch_file
                                     AND status = 'Rejected/Declined'",
                                     array(':batch_file' => $batch_file));
    // If there aren't any?  Hooray!
    if ($failed_transactions->rowCount() == 0) {
      // Watchdog and log.
      $msg = "There are zero failed transactions to process.";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
      write_to_log($msg, 'batch');
      return false;
    }
    else {
      // If there are failed transactions store the order_ids in an array.
      foreach ($failed_transactions as $ft) {
        $oids[] = $ft->order_id;
      }
    }
    // Pull all the transactions related to the order ids we stored above.
    $recurring_payments = db_query('SELECT * FROM commerce_beanstream_recurring_payment
                                    WHERE order_id IN (:order_ids)', array(':order_ids' => $oids));

    // Set process failed transactions flag in the order details array
    // so our hook knows to respect this.
    $order_details['process_failed_transactions'] = TRUE;
  }

  // If there aren't any payments to process write to log and notify.
  if ($recurring_payments->rowCount() == 0) {
    $msg = "There are zero credit card payments to process.";
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
    write_to_log($msg, 'batch');
    return false;
  }

  // Define arrays to be filled.
  $totals        = array();
  $order_ids     = array();
  foreach ($recurring_payments as $recurring) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $recurring->order_id);

    // Make sure we are only processing order statuses user has selected.
    if (in_array($order_wrapper->status->value(), $payment_method['settings']['recurring']['order_statuses'])) {
      // Store order id
      $order_ids[$order_wrapper->uid->value()] = $order_wrapper->order_id->value();

      // Find the total owed and the total paid.
      if ($recurring->amount < 0) {
        $totals[$order_wrapper->uid->value()]['owed'] += $recurring->amount;
      }
      elseif ($recurring->amount > 0) {
        $totals[$order_wrapper->uid->value()]['paid'] += $recurring->amount;
      }
    }
  }

  // Store all order details in one array.  We need to pass this info to a hook if it's being called.
  $order_details['order_ids']   = $order_ids;
  $order_details['totals']      = $totals;

  // Find all implementations of hook_commerce_beanstream_batch_file_order_details_alter and return values.
  if(!empty(module_implements('commerce_beanstream_batch_file_order_details_alter'))) {
    foreach (module_implements('commerce_beanstream_batch_file_order_details_alter') as $module) {

      $function = $module . '_commerce_beanstream_batch_file_order_details_alter';

      // Return the altered $order_details array.
      $order_details = $function($order_details, $payment_method);
    }
  }

  if (!$order_details) {
    $msg = "There are zero orders to proccess! Check logs for details.";
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
    write_to_log($msg . "\nTerminationg batch process...", 'batch');
    return FALSE;
  }

  // Now we have all our values lets start building the csv file.
  foreach($order_details['totals'] as $uid => $total) {

    // Calculate the total amount owed.
    $total_owed = ($order_details['totals'][$uid]['owed'] + $order_details['totals'][$uid]['paid']) * -1;

    // If the total is 0, no transaction is needed.
    if ($total_owed <= 0) {
      continue;
    }

    // Calculate monthly amount.
    $payment = commerce_beanstream_batch_file_calc_monthly($total_owed, $payment_method);

    // Pull existing cards for this specific user.
    $existing_cards = commerce_cardonfile_load_multiple_by_uid($uid, 'commerce_beanstream|commerce_payment_commerce_beanstream');
    if (!empty($existing_cards)) {
      // Take the first card from the existing cards array.
      $existing_card = reset($existing_cards);
      // Extract the remote_id from that card.
      $remote_id = $existing_card->remote_id;
    }
    else {
      $remote_id = '';
      $reference = $order_details['order_ids'][$uid];
      $msg = "User of this order does not have a beanstream payment profile. UID: $uid, REFERENCE: $reference";
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_NOTICE);
      write_to_log($msg, 'batch');
    }

      // Build transaction for this one user, one transaction equals one row in csv file.
    $rows[] = array(
      'Record Type'      => 'C',
      'Transaction Type' => 'P',
      'Adjustment Id'    => '',
      'Card Number'      => '',
      'Card Expiry'      => '',
      'Amount'           => $payment->amount,
      'Reference'        => $order_details['order_ids'][$uid],
      'Card Owner'       => '',
      'Email Address'    => '',
      'Recurring'        => '1',
      'DBA'              => '',
      'Profile Code'     => $remote_id,
    );
  }

  $path     = $payment_method['settings']['recurring']['batch_file'];
  $filename = date('Y-m-d') . '_bs_batch_file.csv';
  $csv_file = fopen( $path . $filename, 'w' ); // Open file.

  // Using fwrite instead of fputcsv to manually set EOL.  Beanstream is using a
  // windows server so standard linux EOL '\n' won't work.
  foreach ($rows as $row) {
    fwrite($csv_file, implode(',', $row) . "\r\n");
  }
  fclose($csv_file);

  if(file_exists($path . $filename)) {
    return $path . $filename;
  }
  else {
    $msg = "The csv you tried to create doesn't exist.";
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
    write_to_log($msg, 'batch');
    return false;
  }
}

/**
 * Calls hook_mail based on what stage the batch file process is on: batch file creation
 * or batch file reporting.
 *
 * @param $payment_method
 * @param $stage
 * @param $result
 * @return mixed
 */
function commerce_beanstream_notify_admin($payment_method, $stage, $result) {

  //module sending the mail.
  $module = 'commerce_beanstream';
  $key = 'monthly_payment';

  // Specify 'to' and 'from' addresses.
  $to = $payment_method['settings']['admin_email'];
  $from = variable_get('site_mail', 'info@example.com');
  $language = language_default();
  $send = true;
  $params = array();
  if($stage == 'batch_file') {
    $params['subject'] = "Monthly credit card batch file";
    $params['message'] = "The " . date('F Y') . " monthly credit card payment batch file process has run with the following response:\r\n";
    $params['message'] .= "\r\n";

    if (count($result) == 0) {
      $params['message'] .= "Beanstream did not receive the batch file. See log below\r\n";
    }
    else {
      $params['message'] .= 'Code: ' . $result->code . "\r\n";
      $params['message'] .= 'Message: ' . $result->message . "\r\n";
      $params['message'] .= 'Batch ID: ' . $result->batch_id . "\r\n";
      $params['message'] .= "Beanstream did not receive the batch file. See log below\r\n";
    }
    $params['message'] .= file_get_contents($payment_method['settings']['recurring']['batch_file'] . date('Y-m-d') . '_bs_batch_create_log.txt');
  }
  elseif($stage == 'report'){
    $params['subject'] = "Monthly credit card report.";

    if(!$result) {
      $params['message'] = "There was an error receiving the report or there wasn't a report to receive, check watchdog, logs or the database.";
    }
    else {
      $params['message'] = "The monthly credit card report has been received and the database has been updated. See below for a brief transaction summary for our monthly credit card payments:\r\n";
      $params['message'] .= "\r\n";
      $params['message'] .= 'Validated/Approved: ' . $result['approved'] . "\r\n";;
      $params['message'] .= 'Rejected/Declined: ' . $result['declined'] . "\r\n";;

      if (isset($result['warning'])) {
        $params['message'] .= 'Transaction Warning:  ' . $result['warning'] . "\r\n";
      }
    }
  }

  // Calls drupal mail which goes to commerce_beanstream_mail in .module file.
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);

  return $result[ 'result' ];
}

/**
 * Helper function: Writes any string to beanstream log files.
 *
 * @param $string
 *  The string to write to the log
 * @param $process
 *  batch or report: writes to specific log files.
 *
 * @return bool|int
 */
function write_to_log($string, $process) {

  // Load payment method to retrieve our secure beanstream directory.
  $payment_method = commerce_payment_method_instance_load('commerce_beanstream|commerce_payment_commerce_beanstream');
  // Set file names based on process.
  if ($process == 'batch') {
    $log_file = $payment_method['settings']['recurring']['batch_file'] . date('Y-m-d') . '_bs_batch_create_log.txt';
  }
  elseif ($process == 'report') {
    $log_file = $payment_method['settings']['recurring']['batch_file'] . date('Y-m-d') . '_bs_report_log.txt';
  }
  else {
    // If process is incorrect write to watchdog and return false.
    watchdog('commerce_beanstream', 'Log file write failed.  Process type doesn\'t exist', array(), WATCHDOG_ERROR);
    return false;
  }

  return file_put_contents($log_file, $string . "\n", FILE_APPEND);
}