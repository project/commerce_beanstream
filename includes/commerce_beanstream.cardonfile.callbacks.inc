<?php

/**
 * Card on File callback: create new card
 *
 * @param array $form
 * @param array $form_state
 * @param array $payment_method
 *   The payment method for the card on file request.
 * @param object $card_data
 *   The commerce_cardonfile entity.
 *
 * @return object/bool
 *   The updated commerce_cardonfile entity or FALSE if there was an error.
 */
function commerce_beanstream_cardonfile_create($form, $form_state, $payment_method, $card_data) {
  $account = user_load($card_data->uid);

  // Prepare Beanstream request data.
  $data = array(
    'trnCardOwner' => $card_data->card_name,
    'trnCardNumber' => $card_data->card_number,
    'trnExpMonth' => $card_data->card_exp_month,
    'trnExpYear' => substr($card_data->card_exp_year, -2),
    'trnCardCvd' => $card_data->card_code,
  );

  // Get billing details and add them to the request data.
  $billing_address = $form_state['commerce_customer_profile']->commerce_customer_address['und'][0];
  $data += array(
    'ordName' => $billing_address['name_line'],
    'ordAddress1' => $billing_address['thoroughfare'],
    'ordAddress2' => $billing_address['premise'],
    'ordCity' => $billing_address['locality'],
    'ordPostalCode' => $billing_address['postal_code'],
    'ordCountry' => $billing_address['country'],
  );

  // Optionally add phone field.
  $phone_field = $form_state['commerce_customer_profile']->field_phone[LANGUAGE_NONE][0];
  $phone_number = $phone_field['value'];
  if ($phone_number) {
    $data['ordPhoneNumber'] = $phone_number;
  }

  if ($billing_address['country'] == 'CA' || $billing_address['country'] == 'US') {
    $data['ordProvince'] = $billing_address['administrative_area'];
  } else {
    $data['ordProvince'] = '--';
  }

  // Get email address and add it to the request data.
  if (isset($account->mail)) {
    $data['ordEmailAddress'] = $account->mail;
  }

  // Look for other cards on file for this user.
  $existing_cards = commerce_cardonfile_load_multiple_by_uid($account->uid, $payment_method['instance_id']);

  // If the user already has cards on file, use the first returned card to get
  // the user's Beanstream profile remote_id.
  $remote_id = FALSE;
  if (!empty($existing_cards)) {
    $existing_card = reset($existing_cards);
    $remote_id = $existing_card->remote_id;
  }

  // If the user already has a Beanstream profile, add the new card to it.
  if ($remote_id) {
    $data['customerCode'] = $remote_id;

    // Optionally set this new card as default in Beanstream.
    if ($card_data->instance_default == 1) {
      $data['function'] = 'DEF';
    }

    // Make the API request and store the response.
    $result = _commerce_beanstream_profile_request('ADD_CARD', $payment_method, $data);
  }
  // Otherwise, create a new Beanstream profile for this user.
  else {
    // Make the API request and store the response.
    $result = _commerce_beanstream_profile_request('N', $payment_method, $data);
  }

  // If the Beanstream request is successful, return the updated
  // commerce_cardonfile entity.
  if (!empty($result) && $result['responseCode'] === "1") {
    // Store only the last four digits of the card in Drupal.
    $card_data->card_number = substr($card_data->card_number, -4);
    $card_data->remote_id = $result['customerCode'];
    watchdog('commerce_beanstream', 'Beanstream Secure Payment Profile @profile_id created and saved to user @uid.', array('@profile_id' => (string) $card_data->remote_id, '@uid' => $account->uid));
    return $card_data;
  }
  // Otherwise, pass the error from Beanstream to the user and log.
  else {
    $error = array(
      '@code' => $result['responseCode'],
      '%message' => $result['responseMessage'],
    );
    #drupal_set_message(t('Payment Profile Creation failed: @code (%message)', $error), 'error');
    watchdog('commerce_beanstream', 'Beanstream Secure Payment Profile creation failed: @code', $error);
    form_set_error('credit_card', t('There was a problem validating or storing your Credit Card information: %message (code=@code). Please try again, or contact us.', $error));
  }

  return FALSE;
}

/**
 * Card on File callback: create new card form
 *
 * @param array $form
 * @param array $form_state
 * @param array $op
 *   The operation being performed - i.e. 'update', 'create'.
 *   This function is currently only called with $op = 'create', but could be
 *   expanded by editing commerce_beanstream_commerce_payment_method_info().
 * @param object $card_data
 *   The commerce_cardonfile entity.
 *
 * @return array
 *   The create new card form.
 */
function commerce_beanstream_cardonfile_create_form($form, &$form_state, $op, $card_data) {
  // Load the credit card helper functions from the Payment module.
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  // Pass along information to the validate and submit handlers.
  $form_state['card'] = $card_data;

  $fields = array(
    'owner' => '',
    'code' => '',
    'type' => array(
      'visa',
      'mastercard',
      'amex',
    ),
  );

  // Default values are not used for 'create' op.
  $defaults = array();

  // Add the credit card form fields.
  $form = commerce_payment_credit_card_form($fields, $defaults);

  // Add a checkbox to set this new card as default.
  $payment_method = commerce_payment_method_load($card_data->payment_method);
  $form['credit_card']['cardonfile_instance_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use as default card for payments with %method', array('%method' => $payment_method['display_title'])),
    '#default_value' => FALSE,
  );

  // Disable the checkbox if we are adding a new card and the user doesn't have
  // any other active cards with the same instance ID. Also disable it, if we
  // are updating the current default card, so the user can't uncheck the
  // checkbox.
  $existing_cards = commerce_cardonfile_load_multiple_by_uid($card_data->uid, $card_data->instance_id, TRUE);
  if (!$existing_cards) {
    $form['credit_card']['cardonfile_instance_default']['#default_value'] = TRUE;
    $form['credit_card']['cardonfile_instance_default']['#disabled'] = TRUE;
  }

  // Create a billing profile object (either loaded from the user's latest
  // existing billing profile or a new, blank profile) and add the billing
  // information fields to the form.
  $profiles = commerce_customer_profile_load_multiple(array(), array('uid' => $card_data->uid));
  $profile = $profiles ? end($profiles) : commerce_customer_profile_new('billing', $card_data->uid);
  $form_state['commerce_customer_profile'] = $profile;
  $form['commerce_customer_profile'] = array();
  field_attach_form('commerce_customer_profile', $profile, $form['commerce_customer_profile'], $form_state);

  // Hide the credit card owner name field because the billing name is used
  // instead.
  $form['credit_card']['owner']['#access'] = FALSE;

  // If the Beanstream Payment Method has been configured to not require the CVD
  // security code, remove it from the form.
  $payment_method_instance = commerce_payment_method_instance_load($card_data->instance_id);
  if (isset($payment_method_instance['settings']['require_cvd']) &&
    $payment_method_instance['settings']['require_cvd'] == 0) {
    unset($form['credit_card']['code']);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add card'),
    '#suffix' => l(t('Cancel'), 'user/' . $card_data->uid . '/cards'),
  );

  return $form;
}

/**
 * Validation callback for 'commerce_beanstream_cardonfile_create_form'.
 * This function replaces the default card on file form validation function.
 *
 * Copies the name associated with the card to the name attribute of the
 * customer's billing profile, so the user doesn't have to enter the name twice.
 */
function commerce_beanstream_cardonfile_create_form_validate($form, &$form_state) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $name = $form_state['values']['commerce_customer_address']['und'][0]['name_line'];
  $form_state['values']['credit_card']['owner'] = $name;

  if (!commerce_payment_credit_card_validate($form_state['values']['credit_card'], array('form_parents' => array('credit_card')))) {
    return FALSE; // validate routine would have form_set_error()'d for us.
  }

  $profile = $form_state['commerce_customer_profile'];
  field_attach_form_validate('commerce_customer_profile', $profile, $form['commerce_customer_profile'], $form_state);

  // Get the card_data (default with only the user id attached).
  $card_data = $form_state['card'];

  // Add data from the form to the card_data.
  $card_data->card_name = $form_state['values']['credit_card']['owner'];
  $card_data->card_exp_month = $form_state['values']['credit_card']['exp_month'];
  $card_data->card_exp_year = $form_state['values']['credit_card']['exp_year'];
  $card_data->card_number = $form_state['values']['credit_card']['number'];
  $card_data->card_code = $form_state['values']['credit_card']['code'];
  $card_data->instance_default = $form_state['values']['credit_card']['cardonfile_instance_default'];

  // Determine the credit card type from the credit card number.
  if (!empty($form_state['values']['credit_card']['number'])) {
    module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
    $card_data->card_type = commerce_payment_validate_credit_card_type($card_data->card_number, array_keys(commerce_payment_credit_card_types()));
  }

  // Invoke the payment method's card create/update callback.
  $payment_method = commerce_payment_method_instance_load($card_data->instance_id);
  $callback = $payment_method['cardonfile']['create callback'];
  if (function_exists($callback)) {
    $callback_return = $callback($form, $form_state, $payment_method, $card_data);

    // The callback's return should either be a cardonfile entity or FALSE.
    if ($callback_return) {
      $form_state['card'] = $callback_return;
    } else {
      return FALSE;
    }
  }
}

/**
 * Submit callback for 'commerce_beanstream_cardonfile_create_form'.
 * This function replaces the default card on file form submit function.
 */
function commerce_beanstream_cardonfile_create_form_submit($form, &$form_state) {

  // Submit the profile to the field attach handlers.
  $profile = $form_state['commerce_customer_profile'];
  field_attach_submit('commerce_customer_profile', $profile, $form['commerce_customer_profile'], $form_state);

  // Save or update the customer profile.
  commerce_customer_profile_save($profile);

  // Submit the card_data to the field attach handlers.
  $card_data = $form_state['card'];
  field_attach_submit('commerce_cardonfile', $card_data, $form, $form_state);
  commerce_cardonfile_save($card_data);

  $form_state['redirect'] = 'user/' . $card_data->uid . '/cards';
}

/**
* Card on file callback: background charge payment
*
* @param object $payment_method
*  The payment method instance definition array.
* @param object $card_data
*   The stored credit card data array to be processed
* @param object $order
*   The order object that is being processed
* @param array $charge
*   The price array for the charge amount with keys of 'amount' and 'currency'
*   If null the total value of the order is used.
*
* @return bool TRUE if the transaction was successful, otherwise FALSE
*/
function commerce_beanstream_cardonfile_charge($payment_method, $card_data, $order, $charge = NULL) {
  $url = $payment_method['settings']['payment_url'];

  // Format order total for transaction.
  if (!isset($charge)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $charge = commerce_line_items_total($wrapper->commerce_line_items);
  }

  //transaction info
  $transaction = commerce_payment_transaction_new('commerce_beanstream', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->message = 'Name: @name';

  //build the request
  $request = array(
    'trnOrderNumber' => $order->order_id,
    'trnAmount' => number_format(commerce_currency_amount_to_decimal($transaction->amount,$transaction->currency_code),2),
    'customerCode' => $card_data->remote_id,
  );

  $bill_details = _commerce_beanstream_order_billing_details($payment_method, $order);
  if (empty($bill_details['ordName'])) {
    drupal_set_message(t('A billing address must be available to perform a transaction'), 'error');
    return FALSE;
  }
  $request = array_merge($request, $bill_details);

  $response = _commerce_beanstream_transaction_request($payment_method, $request);

  //if declined set the tranaction status to false
  if($response['trnApproved'] != '1'  || empty($response['trnApproved'])){
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  } else {
    //otheriwse set transaction status appropriately
    // Set the transaction status based on the type of transaction this was.
    switch ($payment_method['settings']['txn_type']) {
      case COMMERCE_CREDIT_AUTH_ONLY:
        $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
        break;

      case COMMERCE_CREDIT_AUTH_CAPTURE:
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        break;
    }
  }

  //set tranaction variables to those returned from Beanstream
  $transaction->remote_id = $response['trnId'];
  $transaction->remote_status = $response['paymentMethod'];
  $transaction->payload[REQUEST_TIME] = $response;

  //set a msg to return to user
  $message = 'Your transacton has been <b>' . $response['messageText'] . '</b>';

  $transaction->message = $message;

  // save the transaction
  commerce_payment_transaction_save($transaction);

  // if the transaction is declined return false send error to user
  if($response['trnApproved'] != '1'){
    drupal_set_message(t('We received the following error processing your card. Please enter your information again or try a different card.'), 'error');
    drupal_set_message(check_plain($response['messageText']), 'error');
    return FALSE;
  }

  return TRUE;
}

/**
 * Card on file callback: update card information
 *
 * @param array $form
 *   The payment form
 * @param array $form_state
 *   The payment form state
 * @param object $payment_method
 *   The payment method instance definition array
 * @param object $card_data
 *   The new credit card data array to be processed
 * @return bool TRUE if update was successful, or FALSE otherwise.
 */
function commerce_beanstream_cardonfile_update($form, &$form_state, $payment_method, $card_data) {
  $cards = _commerce_beanstream_cardonfile_getcards($payment_method, $card_data->remote_id);

  foreach ($cards as $card) {
    switch ($card['type']) {
      case 'VI': $card_type = 'visa'; break;
      case 'AM': $card_type = 'amex'; break;
      case 'MC': $card_type = 'mastercard'; break;
    }
    if (($card_type == $card_data->card_type) && ($card['number'] == $card_data->card_number)) {
      $data = array(
        'customerCode' => $card_data->remote_id,
        'cardId' => $card['cardId'],
        'trnExpYear' => substr($card_data->card_exp_year, -2),
        'trnExpMonth' => $card_data->card_exp_month,
        'function' => $card_data->instance_default ? "DEF" : "SEC"
      );

      $result = _commerce_beanstream_profile_request('MODIFY_CARD', $payment_method, $data);
      if (!empty($result) && $result['responseCode'] === "1") {
        return TRUE;
      } else {
        return FALSE;
      }
    }
  }
}

/**
 * Card on file callback: delete card information
 *
 * @param array $form
 *   The payment form
 * @param array $form_state
 *   The payment form state
 * @param object $payment_method
 *   The payment method instance definition array
 * @param object $card_data
 *   The credit card data to be deleted
 * @return bool TRUE if card deleted successfully, or FALSE otherwise.
 */
function commerce_beanstream_cardonfile_delete($form, &$form_state, $payment_method, $card_data) {

  // User remote_id to retrieve cards in payment profile on beanstream.
  $cards = _commerce_beanstream_cardonfile_getcards($payment_method, $card_data->remote_id);

  if (count($cards) == 1) {
    // Remove profile and everything within it.
    $data = array('customerCode' => $card_data->remote_id);
    $result = _commerce_beanstream_profile_request_xml('DELETE_PROFILE', $payment_method, $data);
  }
  else {
    // Remove specific card from profile.
    foreach ($cards as $card) {
      // TODO: Should check expiry and cardType as well here.
      $exp_month = substr($card['expiry'], 0, 2);
      $exp_year = substr($card['expiry'], 2, 2);
      // $card['expiry'] == "1115" (11/2015)
      if (($card['owner'] === $card_data->card_name)
        && ($card['number'] === $card_data->card_number)
        && ($exp_month == sprintf("%02d", $card_data->card_exp_month))
        && ($exp_year == substr($card_data->card_exp_year, 2, 2))
      ) {

        $data = array(
          'customerCode' => $card_data->remote_id,
          'cardId'       => $card['cardId'],
        );

        $result = _commerce_beanstream_profile_request_xml('DELETE_CARD', $payment_method, $data);
      }
    }
  }

  // If the profile request was successful return true.
  // OR if we didn't match a card that Beanstream has,
  // we should return TRUE so commerce deletes that card.
  if ((!empty($result) && $result->responseCode == "1") || empty($result)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to lookup a list of cards on file at Beanstream.
 *
 * @param $payment_method Array The payment method instance definition array
 * @param $code String The customer code to lookup
 * @param null $orderNum String The unique order number for this transaction
 * @return array|bool List of cards, or FALSE on error/no cards on file.
 */
function _commerce_beanstream_cardonfile_getcards($payment_method, $code, $orderNum = NULL) {
  $data = array(
    'customerCode' => $code,
  );

  if (isset($orderNum)) {
    $data['trnOrderNumber'] = $orderNum;
  }

  $result = _commerce_beanstream_profile_request_xml('GET_CARDS', $payment_method, $data);

  if (!empty($result) && $result->responseCode == "1") {
    $cards = array();
    foreach ($result->cardNode as $card) {
      if ($card->function->__toString() == 'DEF') {
        $default = TRUE;
      } else {
        $default = FALSE;
      }
      $cards[] = array(
        'cardId' => $card->cardId->__toString(),
        'default' => $default,
        'owner' => $card->owner->__toString(),
        'number' => substr($card->number->__toString(), -4),
        'expiry' => $card->expiry->__toString(),
        'type' => $card->cardType->__toString(),
      );
    }
    return $cards;
  } else {
    return FALSE;
  }
}
