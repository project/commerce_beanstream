<?php

/**
 * Batch file report job queue.  Logs batch file report process in watchdog and separate txt file.
 */
function commerce_beanstream_report_monthly_payments_job_queue() {
  // Initialize log file.
  write_to_log("---------------" . date('Y-m-d H:i:s') . "---------------", 'report');
  // Load payment method.
  $payment_method = commerce_payment_method_instance_load('commerce_beanstream|commerce_payment_commerce_beanstream');
  // Request report from beanstream, returns simpleXML.
  $report = _commerce_beanstream_request_batch_file_report($payment_method);

  if($report){
    $msg = 'Report successfully retrieved from beanstream.';
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
    write_to_log($msg, 'report');

    // Save report to csv file for easy reference.
    _commerce_beanstream_write_report_to_csv($report, $payment_method);
    $msg = 'Full report saved in commerce_beanstream private directory';
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
    write_to_log($msg, 'report');

    $option = drush_get_option('bypass', '');
    if ($option != 'yes') {
      $option = drush_choice(array('yes' => 'Yes'), dt('Would you like to update contacts in CiviCRM?'));
    }
  }
  else{
    $msg = 'Failed to retrieve report from beanstream.';
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
    write_to_log($msg, 'report');
  }

  if(isset($option) && $option != false) {
    // Begin processing report.
    $transaction_summary = commerce_beanstream_process_report($report, $payment_method);

    // Check if transaction summary was populated, if so we successfully updated the DB.
    if (!empty($transaction_summary)) {
      $msg = 'Transaction summary created and database updated';
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
      write_to_log($msg, 'report');
    }
    else { //Log failure if Transaction Summary fails.
      $msg = 'Transaction summary is empty, something weird happened.';
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
      write_to_log($msg, 'report');
    }

    // Send email to beanstream admin to let them know details about the report.
    $email_admin = commerce_beanstream_notify_admin($payment_method, 'report', $transaction_summary);
    // Record delivery success or failure of notification email in watchdog
    if ($email_admin) {
      $msg = 'Notification Email to CC Admin successfully sent.';
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
      write_to_log($msg, 'report');
    }
    else {
      $msg = 'Notification Email to CC Admin failed to send.';
      watchdog('commerce_beanstream', $msg, array(), WATCHDOG_ERROR);
      write_to_log($msg, 'report');
    }
  }
  else {
    $msg = 'Reporting process aborted.';
    watchdog('commerce_beanstream', $msg, array(), WATCHDOG_INFO);
    write_to_log($msg, 'report');
  }
}

/**
 * Processes the xml returned from beanstream and updates the payments in the DB.
 *
 * @param $report
 * @param $payment_method
 * @return array
 */
function commerce_beanstream_process_report($report, $payment_method) {

  // Declare transaction_summary array.
  $transaction_summary = array();

  // Cycle through the xml returned by the API call.
  foreach( $report->row as $transaction ){

    // Set amount to zero if credit card was declined.
    if ($transaction->attributes()['status_id'] == 2) {
      $transaction->attributes()['amount'] = '0';
    }

    // Set status message.
    $transaction->attributes()['status_name'] = (trim($transaction->attributes()['message_name'])) ? $transaction->attributes()['message_name'] : $transaction->attributes()['status_name'];

    //find all implementations of hook_commerce_beanstream_process_report_pre_save and return values.
    if(!empty(module_implements('commerce_beanstream_process_report_pre_save'))) {
      foreach (module_implements('commerce_beanstream_process_report_pre_save') as $module) {

        $function = $module . '_commerce_beanstream_process_report_pre_save';

        $transaction = $function($transaction, $payment_method);
      }
    }

    // Get the payment year of the order from the db.
    $payment_year = db_query("SELECT payment_year FROM commerce_beanstream_recurring_payment WHERE
                              order_id = :order_id AND amount < 0 LIMIT 1",
                              array(':order_id' => $transaction->attributes()['reference']))->fetchField(0);

    // Only convert amount into pennies if amount doesn't equal 0.
    $transaction->attributes()['amount'] = ($transaction->attributes()['amount'] == '0') ? '0' : floatval($transaction->attributes()['amount']) * 100;

    // Insert payment into DB
    $result = commerce_beanstream_save_recurring_credit_card_order(
      $transaction->attributes()['reference'],
      $transaction->attributes()['amount'],
      $payment_year,
      $transaction->attributes()['status_name'],
      $transaction->attributes()['transaction_id'],
      $transaction->attributes()['batch_id']
    );

    if ($result == FALSE) {
      preg_match('/[0-9]{1,15}/', $transaction->attributes()['reference'], $match);
      watchdog('commerce_beanstream', 'Failed to update database with transaction: "order_id: @order_id"', array('@order_id' => $match[0]), WATCHDOG_ERROR);
      write_to_log("Failed to update database with transaction (Order: $match[0])", 'report');
    }

    // Build transaction summary based on beanstream returned value 'status_id'.
    // 1 = Approved.
    // 2 = Declined.
    // 3 = Warning.
    switch ($transaction->attributes()['status_id']) {
      case 1:
        $transaction_summary['approved']++;
        break;
      case 2:
        $transaction_summary['declined']++;
        break;
      case 3:
        $transaction_summary['warning']++;
        break;
    }
  }
  return $transaction_summary;
}

/**
 * Writes report retrieved from beanstream to file.
 * @param $report
 * @param $payment_method
 */
function _commerce_beanstream_write_report_to_csv($report, $payment_method) {
  // Use iterator to define header row for csv
  $x = 0;
  foreach( $report->row as $transaction ){
    $x++;
    if($x == 1) {
      $rows[] = array(
        'merchant_id',
        'batch_id',
        'process_date',
        'credit_id',
        'trans_type',
        'adjustment_id',
        'card_type',
        'card_expiry',
        'amount',
        'reference',
        'card_owner',
        'email_address',
        'recurring',
        'dba_suffix',
        'customer_code',
        'transaction_id',
        'state_id',
        'state_name',
        'message_id',
        'message_name',
      );
    }

    $rows[] = array(
      $transaction->attributes()['merchant_id'],
      $transaction->attributes()['batch_id'],
      $transaction->attributes()['process_date'],
      $transaction->attributes()['credit_id'],
      $transaction->attributes()['trans_type'],
      $transaction->attributes()['adjustment_id'],
      $transaction->attributes()['card_type'],
      $transaction->attributes()['card_expiry'],
      $transaction->attributes()['amount'],
      $transaction->attributes()['reference'],
      $transaction->attributes()['card_owner'],
      $transaction->attributes()['email_address'],
      $transaction->attributes()['recurring'],
      $transaction->attributes()['dba_suffix'],
      $transaction->attributes()['customer_code'],
      $transaction->attributes()['transaction_id'],
      $transaction->attributes()['state_id'],
      $transaction->attributes()['state_name'],
      $transaction->attributes()['status_id'],
      $transaction->attributes()['status_name'],
      $transaction->attributes()['message_id'],
      $transaction->attributes()['message_name'],
    );
  }

  // Write report file to our private cc_monthly directory
  $path     = $payment_method['settings']['recurring']['batch_file'];
  $filename = date('Y-m-d') . '_bs_report_file.csv';
  $csv_file = fopen( $path . $filename, 'w' ); // Open file.

  foreach ($rows as $row) {
    fwrite($csv_file, implode(',', $row) . "\r\n");
  }
  fclose($csv_file);
}