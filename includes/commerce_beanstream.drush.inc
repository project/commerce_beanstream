<?php

include_once('commerce_beanstream.process_batch_file.inc');
include_once('commerce_beanstream.report_batch_file.inc');

/**
 * Implements hook_drush_command().
 *
 * Payments are calculated on a monthly bases.
 * To automate the process you need to set up two cron jobs that trigger these
 * drush commands.
 *
 * One for sending the batch file to beanstream 'drush bsp'
 * One for retrieving the report from beanstream 'drush bsr'.
 * Ideally you would run 'drush bsr' a couple of days after sending the batch file.
 */
function commerce_beanstream_drush_command() {

  $items['beanstream_process'] = array(
    'description' => 'Sends batch file filled with credit card transactions to beanstream.',
    'aliases' => array('bsp'),
  );
  $items['beanstream_report'] = array(
    'description' => 'Retrieves report on the last batch file sent to beanstream.',
    'aliases' => array('bsr')
  );

  return $items;
}

/**
 * Drush function, initiates batch file job queue.
 */
function  drush_commerce_beanstream_beanstream_process() {
  commerce_beanstream_batch_file_job_queue();
}

/**
 * Drush function, initiates monthly payment report job queue.
 */
function drush_commerce_beanstream_beanstream_report() {
  commerce_beanstream_report_monthly_payments_job_queue();
}