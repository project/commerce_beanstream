(function ($) {

    Drupal.behaviors.commerce_beanstream = {
        attach: function (context, settings) {
            // Modifies the beanstream and cardonfile options on the checkout page.
            // Force default card and store card if credit card monthly is chosen.
            $("[id^=edit-commerce-payment-payment-details-credit-card-monthly-credit-card-monthly-payment]").click(function () {

                if ($("[id^=edit-commerce-payment-payment-details-credit-card-monthly-credit-card-monthly-payment]").is(':checked')) {
                    $('#edit-commerce-payment-payment-details-credit-card-cardonfile-store').attr('checked', 'checked').attr('disabled', 'disabled');
                    $(".form-item-commerce-payment-payment-details-cardonfile-instance-default").show();
                    $("[id^=edit-commerce-payment-payment-details-cardonfile-instance-default]").attr('checked', 'checked').attr('disabled', 'disabled');
                }
                else if (!$("[id^=edit-commerce-payment-payment-details-credit-card-monthly-credit-card-monthly-payment]").is(':checked')) {
                    $('#edit-commerce-payment-payment-details-credit-card-cardonfile-store').removeAttr('checked').removeAttr('disabled', 'disabled');
                    $('.form-item-commerce-payment-payment-details-cardonfile-instance-default').hide();
                    $("[id^=edit-commerce-payment-payment-details-cardonfile-instance-default]").removeAttr('checked').removeAttr('disabled', 'disabled');
                }
            });
        }
    };
}(jQuery));
