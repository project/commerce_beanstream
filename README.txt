Commerce Beanstream Payment
===========================

This is a Drupal Commerce (http://drupalcommerce.org) Payment module
implementing simple one-time transactions with CCs in CAD for Beanstream
Payment processor (http://beanstream.com).

Features
========

* Basic Commerce Payment Gateway API implementation, for single-transaction Credit Card payments
(https://drupalcommerce.org/developer-guide/utilizing-core-apis/writing-payment-method-module)
* Card on File module integration, to allow users to store CC details using Beanstream Secure Payment Profiles
* Edit/Delete stored cards from /user profile page

* Recurring monthly credit card payments using the BATCH FILE API.

Installation
============

You need to ensure you have the Phone (http://drupal.org/project/phone) module
installed on your site, and ensure you have at least one Phone field on your
Commerce Billing Profile.

Enable the module, and then configure a new payment method rule
(admin/commerce/config/payment-methods), and configure your merchant settings
in the Beanstream "action"
(admin/commerce/config/payment-methods/manage/commerce_payment_commerce_beanstream_payment/edit).
You will need:

* Merchant ID
* Username/Password (as setup in Beanstream account settings under "order
  settings") security/authentication for API - enables basic CC payments
  Passcode (as setup in Beanstream configuration under "payment profile
* configuration") - enables Card on File/Payment Profiles


(Module does not currently support other authentication mechanisms for the
basic API or Secure Payment Profiles)

You must also select which phone field the module should use to fill the
(required by Beanstream) phone number in transactions, and check which Credit
Card types your merchant account accepts.

You can optionally enable Card on File integration if you have the Card on File
module (http://drupal.org/project/commerce_cardonfile) installed. In this case,
you *must* have Secure Payment Profiles option enabled for your Beanstream
merchant account! If you don't, you will get API errors claiming "bad merchant
ID".

Experimental Recurring Payment Feature
======================================

This version of beanstream_commerce includes an experimental Monthly Recurring Payment Feature.

***Checkout:
Once enabled a 'Monthly Payments' option is added to the order checkout page.  This option displays
the months remaining in the billing year and the approximate amount the customers card will be charged
each of those months.

If recurring monthly payments is selected on an order, it will store that order in the database as a
recurring payment order without charging the credit card.  Also, the payment details (payment amount
and the number of payments) are displayed on the order view page.

***Transaction processing:
To process your recurring transactions you must use a drush command <drush bsp>.  This will calculate
the current month's cost for each recurring order and place them in a batch file with all the appropriate
data needed to charge the credit card. Once beanstream receives the file they will begin processing the
transactions immediately.

It's important to note, if a payment is declined on a previous month the monthly payment will become larger
to compensate for the missed payment.

Each step of the batch file process is recorded in watchdog and once the process is complete an email is sent
to your beanstream admin (defined in the commerce_beanstream settings) to notify them of the results of
the process.

***Transaction reporting:
To retrieve a report on the transactions in the most recent batch file use <drush bsr>. This command will get
the report from beanstream, process the xml and store the transaction details in the database. Finally
it will email beanstream admin with a transaction summary.

We used a drush command to trigger these processes for automation via a cronjob.


Hooks
=====

There are four hooks built into this update.

1. hook_commerce_beanstream_checkout_calc_monthly_alter($order, $payment, $payment_method)

This hook allows developers to alter the monthly payment object '$payment_info' displayed on the checkout page and order view page.
This object contains all the critical information needed to calculate the monthly price.  So if you wish to alter the way monthly
payments are calculated this is the hook to use.

2. hook_commerce_beanstream_batch_file_calc_monthly_alter($total_owed, $payment, $payment_method)

Similar to the hook above, this hook allows you to alter the monthly payment object '$payment_info' which stores the payment data
appended to the batch file, if you alter the payment_info object in hook_commerce_beanstream_checkout_calc_monthly_alter, you'll
need to do it here as well.

3. hook_commerce_beanstream_batch_file_order_details_alter($order_details, $payment_method)

This hook allows you to alter the details of the order before placing them in the batch file.
The $order_details array is a multi dimensional array that contains three arrays.  One for order ids, one for
order purchase dates, and one for the amount still owed on the order.

5. hook_commerce_beanstream_process_report_pre_save($row, $payment_method)

This hook allows you to access the data from the report retrieved from beanstream via the API before it is saved
to the database. The $row object is parsed from XML.  To access a value you must use the attributes class.  ie.
$row->attributes()['reference'].


**IMPORTANT**
Currently, this feature will only build transactions if the recurring payments were purchased in the current
billing year. Once the billing year is over the recurring payment stops.


SETUP
=====
Along with the initial installation to setup commerce_beanstream you will need:

3 more pass codes that can be found in the account settings of your beanstream profile.
* The Batch File Upload Pass Code
* The Report Request Passcode
* Recurring Payments Enabled

The following settings can be modified at the following url:
/admin/commerce/config/payment-methods/manage/commerce_payment_commerce_beanstream/edit/
* Enable Recurring Payments
* Last month of recurring billing cycle set
* Monthly billing day set.
* You will also need to set the path to the batch file storage.
  Please do not save these files in a public folder!

Once you've set this up properly you should see an option to make monthly credit card payments on the checkout page.